import {gql} from 'apollo-boost';

export const defaults = {
  likedPhotos: []
}

export const resolvers = {
  Image: {
    isLiked: () => false
  },
  Mutation: {
    toggleLikedPhoto: (_, { id }, { cache, getCacheKey }) => {
      console.log('cache', cache)
      const fragment = gql`
        fragment isLiked on Image {
          isLiked,
          url
        }
      `;
      const fragmentId = getCacheKey({ id, __typename: "Image" });
      console.log('fragmentId', fragmentId)
      const photo = cache.readFragment({
        fragment,
        id: fragmentId
      });
      console.log('photo', photo)

      cache.writeData({
        id: fragmentId,
        data: {
          ...photo,
          isLiked: !photo.isLiked
        }
      });

      const query = gql`
        {
          likedPhotos @client {
            url,
            id
          }
        }
      `;
      const { likedPhotos } = cache.readQuery({ query });

      console.log('likedPhotos', likedPhotos)

      const data = {
        likedPhotos: photo.isLiked
        ? likedPhotos.filter(photo => photo.id !== id)
        : likedPhotos.concat([{url: photo.url, id, __typename: 'LikedPhoto'}])
      };

      cache.writeData({ data });
      return data;
    }
  }
}

