import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { AppRegistry, View } from "react-native-web";


AppRegistry.registerComponent("App", () => App);
AppRegistry.runApplication("App", { rootTag: document.getElementById("root") });