import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native-web';

import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';

import { Dog } from '../Dog';
import Header from '../Header';

const GET_LIKED_PHOTOS = gql`
  query {
    likedPhotos @client {
      url
      id
    }
  }
`;

const Likes = () => (
  <View>
    <Header text='Likes' />
    <Query query={GET_LIKED_PHOTOS}>
      {({ loading, error, data }) => {
        console.log('data', data)
        return (
          <ScrollView>
            <View style={styles.container}>
              {data.likedPhotos.map(dog => {
                return <Dog key={dog.id} {...dog} />
              })}
            </View>
          </ScrollView>
        )
      }}
    </Query>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

export default Likes