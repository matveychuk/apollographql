import React from 'react';
import { View, StyleSheet, ScrollView, Text } from 'react-native-web';

import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import Header from '../Header';
import { DogWithLikes } from '../Dog';

export const GET_DOG = gql`
  query getDogByBreed($breed: String!) {
    dog(breed: $breed) {
      images {
        url
        id
        isLiked @client
      }
    }
  }
`;

const Detail = ({ match: { params: { id, breed } } }) => {
  console.log('breed', breed)
  return(
  <View style={styles.container}>
    <Header text={breed} />
    <Query query={GET_DOG} variables={{ breed }}>
      {({ loading, error, data }) => {
        if (loading) return <p>Fetching...</p>;
        if (error) return <p>Error...</p>
        return (
          <ScrollView>
            <View style={styles.container}>
              {data.dog.images.map((item) => {
                return (
                  <DogWithLikes {...item} key={item.id} />
                )
              })}

            </View>
          </ScrollView>
        )
      }}
    </Query>
  </View>
)}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

export default Detail;