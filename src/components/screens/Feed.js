import React from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native-web';
import { Link } from 'react-router-dom';

import gql from "graphql-tag";
import { Query } from "react-apollo";

import Header from '../Header';
import { Dog } from '../Dog';

const GET_DOGS = gql`
  {
    dogs {
      id
      breed,
      displayImage
    }
  }
`;

const Feed = () => (
  <View>
    <Header />
    <Query query={GET_DOGS}>
      {({ loading, error, data }) => {
        if (loading) return <Text>Loading...</Text>
        if (error) return <Text>error...</Text>

        // console.log('data', data)
        return (
          <ScrollView >
            <View style={styles.container}>
              {data.dogs.map(dog => (
                <Link key={dog.id} to={{
                  pathname: `/apollographql/${dog.breed}/${dog.id}`,
                  state: { id: dog.id }
                }}>
                  <Dog {...dog} url={dog.displayImage} key={dog.id} />
                </Link>
              ))}
            </View>
          </ScrollView>
        )
      }}
    </Query>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

export default Feed;