import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native-web';
import { Link } from 'react-router-dom';
import Heart from '../components/Heart';

const Header = ({ text }) => (
  <View style={styles.container}>
    <Link to='/apollographql'>
      <Image source='https://avatars0.githubusercontent.com/u/17189275?s=400&v=4' style={styles.apollo} />
    </Link>
    <Text style={styles.text}>{text || 'Pupstagram'}</Text>
    <Link to='/apollographql/likes'>
      <Heart color='white' width={40} height={40} />
      </Link>
  </View>

)

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#17cbc4',
    flexDirection: 'row'
  },
  apollo: {
    height: 50,
    width: 50
  },
  text: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold'
  }
})

export default Header;