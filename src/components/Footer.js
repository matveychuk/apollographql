import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import Link from './Link';

const GET_VISIBILITY_FILTER = gql`
{
  visibitityFilter @client
}
`

const FilterLink = ({ filter, children }) => {
  <Query query={GET_VISIBILITY_FILTER}>
    {({data, client}) => (
      <Link
        onClick={() => client.writeData({data: {visibilityFilter: filter}})}
        active={data.visibilityFilter === filter}
      >
      {children}
      </Link>
    )}
  </Query>
}

const Footer = () => (
  <p>
    Show: <FilterLink filter='SHOW_ALL'>All</FilterLink>{', '}<FilterLink filter='SHOW_ACTIVE'>All</FilterLink>
  </p>
)