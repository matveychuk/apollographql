import React from 'react';
import gql from "graphql-tag";
import { Query } from "react-apollo";

const GET_DOG_PHOTO = gql`
  query dog($breed: String!) {
    dog(breed: $breed) {
      id
      displayImage
    }
  }
`;

const DogPhoto = ({ breed }) => (
  <Query     
  query={GET_DOG_PHOTO}
  variables={{ breed }}
  // pollInterval={5000}
  notifyOnNetworkStatusChange
  // pollInterval={500}
  >
    {({ loading, error, data, refetch, networkStatus }) => {
      if (networkStatus === 4) return "Refetching!";
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      return (
        <div>
          <img
            src={data.dog.displayImage}
            style={{ height: 100, width: 100 }}
          />
          <button onClick={() => refetch({breed: 'borzoi'})}>Refetch!</button>
        </div>
      );
    }}
  </Query>
);

export default DogPhoto;