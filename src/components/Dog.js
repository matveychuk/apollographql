import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native-web';
import { Mutation } from 'react-apollo';

import { gql } from "apollo-boost";
import Heart from "./Heart";

const TOGGLE_LIKED_PHOTO = gql`
  mutation toggleLikedPhoto($id: String!) {
    toggleLikedPhoto(id: $id) @client
  }
`;

export const Dog = ({ breed, url, id }) => {
  return (
  <View style={styles.container} key={id}>
    <Image style={styles.image} source={url} />
    {breed && <Text style={styles.text}>{breed}</Text>}
  </View>
)
}

export const DogWithLikes = ({ url, id, isLiked}) => {
  return (
    <Mutation mutation={TOGGLE_LIKED_PHOTO} variables={{ id: id }}>
      {toggleLike => (
        <View style={styles.container}>
          <Image source={url} style={styles.image} />
          <Heart
            color={isLiked ? "pink" : "#17cbc4"}
            height={60}
            width={30}
            onPress={toggleLike}
          />
        </View>
      )}
    </Mutation>
  )
}

const styles = StyleSheet.create({
  container: {
    margin: 50,
    alignItems: 'center'
  },
  image: {
    width: 150,
    height: 150,
    marginBottom: 20
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'Luckiest Guy'
  }
})