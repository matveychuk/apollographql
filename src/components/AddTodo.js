import gql from 'graphql-tag';
import React, { Component } from 'react';
import { Mutation } from 'react-apollo';

const ADD_TODO = gql`
  mutation addTodo($type: String!) {
    addTodo(type: $type) {
      id
      type
    }
  }
`

const GET_TODOS = gql`
{
  todos {
    id
    type
  }
}
`

const AddTodo = () => {
  let input;

  return (
    <Mutation 
    mutation={ADD_TODO}
    update={(cache, {data, data: {addTodo}}) => {
      console.log('cache', cache, 'data', data, 'addtodo', addTodo)
      const { todos} = cache.readQuery({query: GET_TODOS});
      cache.writeQuery({
        query: GET_TODOS,
        data: {todos: todos.concat([addTodo])}
      })
      console.log('todos', todos)
    }}
    >
      {(addTodo) => (
        <div>
          <form
            onSubmit={e => {
              e.preventDefault();
              console.log('input', input.value)
              addTodo({ variables: { type: input.value } });
              input.value = '';
            }}
          >

            <input ref={node => input = node} />
            <button type='submit'>Add Todo</button>
            </form>
        </div>
      )}
    </Mutation>
  )
}

export default AddTodo;