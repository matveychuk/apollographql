import React, { Component } from 'react';
import './App.css';
import { ApolloProvider } from "react-apollo";
import Feed from './components/screens/Feed';
import Detail from './components/screens/Detail';
import Likes from './components/screens/Likes';
import { defaults, resolvers } from './resolvers'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { View } from "react-native-web";

import ApolloClient from "apollo-boost";

const ENV_REF = '/apollographql/'

const client = new ApolloClient({
  uri: `https://nx9zvp49q7.lp.gql.zone/graphql`,
  clientState: {
    defaults,
    resolvers
  }
});



class App extends Component {

  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <View>
            <Switch>
              <Route exact path={`${ENV_REF}`} component={Feed} />
              <Route path={`${ENV_REF}likes`} component={Likes} />
              <Route path={`${ENV_REF}:breed/:id`} component={Detail} />
            </Switch>
          </View>
        </Router>
      </ApolloProvider>
    )
  }
};


export default App;
